<?php include_once APPROOT . "/views/partials/header.php"; ?>

<?php

foreach ($data["posts"] as $post) {
?>
    <div class='card card-body mb-3'>
        <h4><?= $post->title ?></h4>
        <a href='<?= URLROOT  ?>/posts/show/<?= $post->id ?>' class='btn btn-dark'>More</a>
    </div>
<?php
}

?>

<?php include_once APPROOT . "/views/partials/footer.php"; ?>