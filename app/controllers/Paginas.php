<?php
//Page Controller
class Paginas extends Controller
{

    //Método para mostrar la página principal
    public function index()
    {
        $data = array(
            'titulo' => 'Framework de Iriome'
        );
        $this->view('paginas/index', $data);
    }

    //Método para mostrar la página de contacto
    public function contacto($id = null)
    {
        echo "Página de contacto";
        echo "<br>";
        echo $id;
    }

    //Método para mostrar la página de acerca de
    public function about()
    {
        $this->view('paginas/about');
    }
}
