<?php 
//Post Controller
class Posts extends Controller {
    // Constructor
    public function __construct() {
        // Load the model
        $this->postModel = $this->model('Post');
        // Load the view
        //$this->view = new View();
    }

    // Index
    public function index() {
        // Get posts
        $posts = $this->postModel->getPosts();
        $data = [
            'titulo' => 'Posts',
            'posts' => $posts
        ];
        // Load view
        $this->view('posts/index', $data);
    }
}